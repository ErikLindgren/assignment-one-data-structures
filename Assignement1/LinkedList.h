#pragma once

#include <iostream>
#include <vld.h> 

#pragma warning( once : 4127 )

template<typename T>
struct LinkedListNode
{
	T value;
	LinkedListNode *nextNode;
};

template<typename T>
class LinkedList
{
public:
	LinkedList() { firstNode = nullptr; };
	~LinkedList() { clear(); };

   void push_back(T value)
   {
      LinkedListNode<T> *tempNode;
      tempNode = findLastNode();
      if (tempNode == nullptr) {
         tempNode = createNewEmptyNode(tempNode);
         tempNode->value = value;
         firstNode = tempNode;
      }
      else
      {
         LinkedListNode<T> *node = nullptr;
         node = createNewEmptyNode(node);

         node->value = value;
         tempNode->nextNode = node;

      }
   };
	void push_front(T value)
	{
		LinkedListNode<T> *node = nullptr;
		node = createNewEmptyNode(node);
		node->nextNode = firstNode;
		node->value = value;

		firstNode = node;
	};
	
	void pop_back() 
	{
		if (firstNode->nextNode == nullptr){
			delete firstNode;
			firstNode = nullptr;
			return;
		}

		LinkedListNode<T> *tempNode = firstNode;
		while (tempNode->nextNode->nextNode != nullptr)
		{
			tempNode = tempNode->nextNode;
		}
		//deleting last node to prevent leakage
		delete tempNode->nextNode;
		tempNode->nextNode = nullptr;
	};
   void pop_front()
   {
      if (firstNode->nextNode == nullptr) {
         delete firstNode;
         firstNode = nullptr;
      }
      else
      {

         //make sure pointers are not leaking
         LinkedListNode<T> *tempNode = nullptr;
         tempNode = firstNode;

         firstNode = firstNode->nextNode;

         delete tempNode;
         tempNode = nullptr;
      }
   };

	LinkedListNode<T>* find(T value) {
		LinkedListNode<T> *nextNode = nullptr;
		if (firstNode == nullptr)
			return false;
		nextNode = firstNode;

		while (true)
		{
			if (nextNode->nextNode == nullptr)
				return nullptr;
			if (nextNode->value == value)
				return nextNode; //Found last node

			nextNode = nextNode->nextNode; //Get the next node in the list
		}
	};
	
	void clear() {
      while (firstNode != nullptr) {
			pop_front();
      }
	};
	int size() {
		if (firstNode == nullptr)
			return 0;

		LinkedListNode<T> *tempNode = firstNode;

		int size = 0;
		while (true)
		{
			size++;
			if (tempNode->nextNode == nullptr)
				return size; //Found last node

			tempNode = tempNode->nextNode; //Get the next node in the list
		}
	};

   int at(int index)
   {
      LinkedListNode<T>* tempNode = firstNode;
      for (int i = 0; i < index; i++)
      {
         tempNode = tempNode->nextNode;
      }
      return tempNode->value;
   }

	void outputList() {
		LinkedListNode<T> *nextNode = nullptr;
		if (firstNode == nullptr)
			firstNode = createNewEmptyNode(firstNode);
		nextNode = firstNode;

		while (true)
		{
			if (nextNode->nextNode == nullptr)
				break; //Found last node

			std::cout << nextNode->value << " ";
			nextNode = nextNode->nextNode; //Get the next node in the list
		}
      std::cout << nextNode->value << " ";
	}

private:
	LinkedListNode<T> *firstNode;

	LinkedListNode<T>* findLastNode()
	{
		LinkedListNode<T> *nextNode = nullptr;
      if (firstNode == nullptr)
         return nullptr;
			//firstNode = createNewEmptyNode(firstNode);
		nextNode = firstNode;

		while (true)
		{
			if (nextNode->nextNode == nullptr)
				return nextNode; //Found last node

			nextNode = nextNode->nextNode; //Get the next node in the list
		}
	}
	LinkedListNode<T>* createNewEmptyNode(LinkedListNode<T> *node)
	{
		node = new LinkedListNode<T>;
		node->nextNode = nullptr;
		node->value = 0;
		return node;
	}
};
