#include "LinkedList.h"
#include "BinarySearchTree.h"
#include "unit.hpp"
#include <string.h>
#include <cassert>
#include <string>
#include <iostream>
#include <vld.h> 

bool testingLinkedList()
{
   std::cout << std::endl << "Starting Linked List testing..." << std::endl << "___________________________________________" << std::endl;
	
   bool passed = true;
	LinkedList<int>* list = new LinkedList<int>;
   std::vector<int>* controlList = new std::vector<int>; // used to verify the list


	//adding numbers to the linked list and a std::Vector for verification
	for (int i = 0; i < 20; i++)
	{
      if (i % 2 == 0)
			list->push_back(i);
      else 
         list->push_front(i);
	}
   controlList->push_back(19);
   controlList->push_back(17);
   controlList->push_back(15);
   controlList->push_back(13);
   controlList->push_back(11);
   controlList->push_back(9);
   controlList->push_back(7);
   controlList->push_back(5);
   controlList->push_back(3);
   controlList->push_back(1);
   controlList->push_back(0);
   controlList->push_back(2);
   controlList->push_back(4);
   controlList->push_back(6);
   controlList->push_back(8);
   controlList->push_back(10);
   controlList->push_back(12);
   controlList->push_back(14);
   controlList->push_back(16);
   controlList->push_back(18);

	//comparing size as a primary verification
	if (!verify(20, list->size(), "Size function test"))
		passed = false;

   if (!verify(5, list->find(5)->value, "Find test"))
      passed = false;

	//Looping through and looking for all of the expected numbers and verifying them against the std::vector
   if (!verify(controlList, list, "Push_back & push_front test"))
      passed = false;

	//poping front and back then checking the numbers again
	list->pop_back();
	list->pop_front();
   controlList->pop_back();
   controlList->erase(controlList->begin());

   if (!verify(controlList, list, "Pop_font & pop_back test"))
      passed = false;

	//Clearing list and checking with size that has hopefully been confirmed as working prior to this test 
	list->clear();
	if (!verify<int>(0, list->size(), "Clear test"))
		passed = false;

   delete controlList;
   controlList = nullptr;
   //list->clear();
	delete list;
	list = nullptr;
   std::string outcome = "WITH ERROR!!";
   if (passed)
      outcome = "successfully!";
   std::cout << "____________________________________________" << std::endl << "Linked list testing finished " << outcome << std::endl << "============================================" << std::endl << std::endl;

	return passed;
}

bool testingBinarySearchTree()
{
	std::cout << "Starting Binary Search Tree testing..." << std::endl << "___________________________________________"<< std::endl;

	bool passed = true;
	BinarySearchTree<int> *binarySearchTree = new BinarySearchTree<int>;

	//Inserting numbers
	binarySearchTree->insert(5);
	binarySearchTree->insert(8);
	binarySearchTree->insert(8);
	binarySearchTree->insert(2);
	binarySearchTree->insert(1);
	binarySearchTree->insert(3);
	binarySearchTree->insert(7);
	binarySearchTree->insert(6);
	binarySearchTree->insert(9);

	if (!verify<int>(binarySearchTree->find(5)->value, 5, "find & insert test part 1"))
		passed = false;
	if (!verify<int>(binarySearchTree->find(3)->value, 3, "find & insert test part 2"))
		passed = false;


	//setting up LinkedLists with expected traversal information
	LinkedList<int>* preOrderTraversalList = new LinkedList<int>;
	preOrderTraversalList->push_back(5);
	preOrderTraversalList->push_back(2);
	preOrderTraversalList->push_back(1);
	preOrderTraversalList->push_back(3);
	preOrderTraversalList->push_back(8);
	preOrderTraversalList->push_back(7);
	preOrderTraversalList->push_back(6);
	preOrderTraversalList->push_back(9);

	LinkedList<int>* inOrderTraversalList = new LinkedList<int>;
	inOrderTraversalList->push_back(1);
	inOrderTraversalList->push_back(2);
	inOrderTraversalList->push_back(3);
	inOrderTraversalList->push_back(5);
	inOrderTraversalList->push_back(6);
	inOrderTraversalList->push_back(7);
	inOrderTraversalList->push_back(8);
	inOrderTraversalList->push_back(9);

	LinkedList<int>* PostOrderTraversalList = new LinkedList<int>;
	PostOrderTraversalList->push_back(1);
	PostOrderTraversalList->push_back(3);
	PostOrderTraversalList->push_back(2);
	PostOrderTraversalList->push_back(6);
	PostOrderTraversalList->push_back(7);
	PostOrderTraversalList->push_back(9);
	PostOrderTraversalList->push_back(8);
	PostOrderTraversalList->push_back(5);

	LinkedList<int> *list = binarySearchTree->preOrderTraversal();
	if(!verify(preOrderTraversalList, list, "Pre Order Traversal"))
		passed = false;

   delete list;
   list = nullptr;
	
   list = binarySearchTree->inOrderTraversal();
	if(!verify(inOrderTraversalList, list, "In Order Traversal"))
		passed = false;
   
   delete list;
   list = nullptr;
   
   list = binarySearchTree->postOrderTraversal();
	if (!verify(PostOrderTraversalList, list, "Post Order Traversal"))
		passed = false;

   delete list;
   list = nullptr;
   
   if (!verify(PostOrderTraversalList->size(), binarySearchTree->size(), "Size"))
		passed = false;
	binarySearchTree->clear();
	if (!verify(0, binarySearchTree->size(), "Clear"))
		passed = false;

   
	delete binarySearchTree;
   binarySearchTree = nullptr;
   delete preOrderTraversalList;
   preOrderTraversalList = nullptr;
   delete inOrderTraversalList;
   inOrderTraversalList = nullptr;
   delete PostOrderTraversalList;
   PostOrderTraversalList = nullptr;


   std::string outcome = "WITH ERROR!!";
   if (passed)
      outcome = "successfully!";
   std::cout << "_________________________________________________" << std::endl << "Binary Search tree testing finished "<< outcome << std::endl 
      << "=================================================" << std::endl << std::endl;

	return passed;
}
void main()
{
	bool passedAllTest = true;
	bool passedLinkedList;
	bool passedBinarySearchList;

	passedLinkedList = testingLinkedList();
	passedBinarySearchList = testingBinarySearchTree();

	if (!passedLinkedList || !passedBinarySearchList)
		passedAllTest = false;

	if (passedAllTest)
		std::cout << "==============================" << std::endl << "|  Passed all of the tests!  |" << std::endl << "==============================" << std::endl << std::endl;
	else
		std::cout << "===================================" << std::endl << "|  ALERT! SOME TEST DID NOT PASS! |" << std::endl << "===================================" << std::endl << std::endl;

	system("Pause");

}