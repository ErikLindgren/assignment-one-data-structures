#pragma once

#include <cassert>
#include <iostream>
#include "LinkedList.h"
#include <vld.h> 

#pragma warning( once : 4127 )

template<typename TValue>
struct BinarySearchNode
{
	TValue value;
	BinarySearchNode *parentNode;
	BinarySearchNode *lowerChildNode;
	BinarySearchNode *higherChildNode;
};

template<typename TValue>
class BinarySearchTree
{
public:
   BinarySearchTree() { firstNode = nullptr; };
   ~BinarySearchTree() { clear(); };

   void insert(TValue value) {

      BinarySearchNode<TValue> *newNode = nullptr;
      if (firstNode == nullptr) {
         firstNode = createNode(newNode, value);
         return; // Added the node, returning
      }

      newNode = createNode(newNode, value);
      BinarySearchNode<TValue> *nextNode = firstNode;
      while (true)
      {
         if (newNode->value == nextNode->value) {
            delete newNode;
            newNode = nullptr;
            return; //Skipping node, returning
         }
         else if (newNode->value > nextNode->value)
         {
            if (nextNode->higherChildNode != nullptr)
               nextNode = nextNode->higherChildNode; //Getting the next node
            else
            {
               //Found the slot, setting up parentship
               newNode->parentNode = nextNode;
               nextNode->higherChildNode = newNode;
               return; //All done, found the node connection
            }
         }
         else if (newNode->value < nextNode->value)
         {
            if (nextNode->lowerChildNode != nullptr)
               nextNode = nextNode->lowerChildNode; //Getting the next node

            else
            {
               //Found the slot, setting up parentship
               newNode->parentNode = nextNode;
               nextNode->lowerChildNode = newNode;
               return; //All done, found the node connection
            }
         }
      }
   };
   BinarySearchNode<TValue>* find(TValue value) 
   {
      if (firstNode == nullptr)
         return nullptr;
      BinarySearchNode<TValue>* nextNode = firstNode;
      while (true)
      {
         if (nextNode->value == value)
         {
            return nextNode;
         }
         else if (nextNode->value <= value && nextNode->higherChildNode != nullptr)
            nextNode = nextNode->higherChildNode;
         else if (nextNode->value >= value && nextNode->lowerChildNode != nullptr)
            nextNode = nextNode->lowerChildNode;
      }
   };

   void clear() 
   {
	   clear(firstNode);
	   firstNode = nullptr;
   };
   int size() 
   {
	   int size = 0;
	   sizeofTree(firstNode, size);
	   return size;
   };

   LinkedList<int>* preOrderTraversal()
   {
	   LinkedList<int>* traversedlist = new LinkedList<int>;
	   traversal_pre_order(firstNode, traversedlist);
	   return traversedlist;
   }
   LinkedList<int>* inOrderTraversal()
   {
	   LinkedList<int>* traversedlist = new LinkedList<int>;
	   traversal_in_order(firstNode, traversedlist);
	   return traversedlist;
   }
   LinkedList<int>* postOrderTraversal()
   {
	   LinkedList<int>* traversedlist = new LinkedList<int>;
	   traversal_post_order(firstNode, traversedlist);
	   return traversedlist;
   }
private:
	BinarySearchNode<TValue> *firstNode;

   BinarySearchNode<TValue>* createNode(BinarySearchNode<TValue> *node, TValue value) {
      node = new BinarySearchNode<TValue>;
      node->parentNode = nullptr;
      node->higherChildNode = nullptr;
      node->lowerChildNode = nullptr;
      node->value = value;
      return node;
   }
   
   //Recursive clear
   void clear(BinarySearchNode<TValue>* node)
   {
	   if (node == nullptr)
		   return;
	   clear(node->lowerChildNode);
	   clear(node->higherChildNode);
	   delete node;
   }
   //Recursive size
   void sizeofTree(BinarySearchNode<TValue>* node, int &size)
   {
	   if (node)
	   {
		   size++;
		   sizeofTree(node->lowerChildNode, size);
		   sizeofTree(node->higherChildNode, size);
	   }
   }
   //Recursive traversals
   void traversal_pre_order(BinarySearchNode<TValue>* node, LinkedList<int> *traversedlist)
   {
	   if (node)
	   {
		   traversedlist->push_back(node->value);
		   traversal_pre_order(node->lowerChildNode, traversedlist);
		   traversal_pre_order(node->higherChildNode, traversedlist);
	   }
   }
   void traversal_in_order(BinarySearchNode<TValue>* node, LinkedList<int> *traversedlist)
   {
	   if (node)
	   {
		   traversal_in_order(node->lowerChildNode, traversedlist);
		   traversedlist->push_back(node->value);
		   traversal_in_order(node->higherChildNode, traversedlist);
	   }
   }
   void traversal_post_order(BinarySearchNode<TValue>* node, LinkedList<int> *traversedlist)
   {
	   if (node)
	   {
		   traversal_post_order(node->lowerChildNode, traversedlist);
		   traversal_post_order(node->higherChildNode, traversedlist);
		   traversedlist->push_back(node->value);
	   }
   }
};