// unit.hpp

#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <vld.h> 
#include <vld.h> 
#include "LinkedList.h"

template <class T>
bool are_equal(T a, T b)
{
	return a == b;
}
template <class T>
bool are_equal(LinkedList<T>* a, LinkedList<T>* b)
{
	
	for (int i = 0; i < b->size(); i++)
	{
		if (a->at(i) != b->at(i))
			return false;
	}
	return true;
}

template <class T>
bool are_equal(std::vector<T>* a, LinkedList<T>* b)
{

   for (int i = 0; i < b->size(); i++)
   {

      if (a->at(i) != b->at(i))
         return false;
   }
   return true;
}

template <class T>
bool verify(T expected, T got, const std::string& message)
{
	if (are_equal(expected, got))
	{
		std::cout << "Passed: " << message << std::endl;
		return true;
	}
	std::cout << "Failed! Expected: " << expected << " Got: " << got <<
		" - " << message << std::endl;
	return false;
}

template <class T>
bool verify(LinkedList<T>* expected, LinkedList<T>* got, const std::string& message)
{
	if (are_equal(expected, got))
	{
		std::cout << "Passed: " << message << std::endl;
		return true;
	}
	std::cout << "Failed!" << std::endl << "Expected: ";
	expected->outputList();
	std::cout << std::endl << "Got:      ";
	got->outputList(); 
	std::cout << " - " << message << std::endl;
	return false;
}

template <class T>
bool verify(std::vector<T>* expected, LinkedList<T>* got, const std::string& message)
{
   if (are_equal(expected, got))
   {
      std::cout << "Passed: " << message << std::endl;
      return true;
   }
   std::cout << "Failed!" << std::endl << "Expected: ";
   
   for (auto it = expected->begin(); it != expected->end(); it++) {
      std::cout << *it << " ";
   }
   
   std::cout << std::endl << "Got:      ";
   got->outputList();
   std::cout << " - " << message << std::endl;
   return false;
}
